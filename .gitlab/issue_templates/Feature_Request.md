###Summary

(Summarize the requested feature encountered concisely)


###Affected functions/datasets/objects

(If possible, mention all functions/datasets/objects that this feature would have an effect on)

/label ~feature-request
/cc @zych