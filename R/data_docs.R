#' Documentation for the example siamcat object in the data folder
#'
#' Reduced version of the CRC dataset in inst/extdata, containing 100 features
#' (15 associated features at 5\% FDR in the original dataset and 85 random
#' other features) and 141 samples, saved after the complete SIAMCAT
#' pipelinehas been run.
#' Therefore, contains entries in every siamcat-object slot,
#' e.g, \code{eval_data} or \code{data_split}. Mainly used for running
#' the examples in the function documentation
#'
#' @name siamcat_example
#'
#' @docType data
#'
#' @keywords data
NULL

#' Documentation for the example feature object in the data folder
#'
#' Feature matrix (as data.frame) for the CRC dataset, containing 141 samples
#' and 1754 bacterial species (features).
#'
#' @name feat.crc.zeller
#'
#' @docType data
#'
#' @keywords data
NULL

#' Documentation for the example metadata object in the data folder
#'
#' Metadata (as data.frame) for the CRC dataset, containing 6 variables (e.g.
#' Age or BMI) for 141 samples.
#'
#' @name meta.crc.zeller
#'
#' @docType data
#'
#' @keywords data
NULL
