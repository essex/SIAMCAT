# SIAMCAT - Statistical Inference of Associations between Microbial Communities And host phenoType

## Overview
`SIAMCAT` is part of the suite of computational microbiome analysis tools
hosted at [EMBL](https://www.embl.org) by the groups of
[Peer Bork](https://www.embl.de/research/units/scb/bork/index.html) and
[Georg Zeller](https://www.embl.de/research/units/scb/zeller/index.html). Find
out more at [EMBL-microbiome tools](http://microbiome-tools.embl.de/).

## Starting with SIAMCAT
In order to start with SIAMCAT, you need to install it from Bioconductor:
```
source("https://bioconductor.org/biocLite.R")
biocLite("SIAMCAT")
```

There are few manuals that will kick-start you and help you analyse your
data with SIAMCAT:
```
browseVignettes("SIAMCAT")
```

## Contact 

If you run into any issue:
- mail Georg Zeller (mailto: zeller@embl.de)
or
- create an issue in this repository
or
- ask at the [SIAMCAT support group](https://groups.google.com/forum/#!forum/siamcat-users)

## Citation

If you use `SIAMCAT`, please cite us by using

```
citation("SIAMCAT")
```

or by

> Zych K, Wirbel J, Essex M, Breuer K, Karcher N, Costea PI, Sunagawa S, Bork P,
Zeller G (2018). _SIAMCAT: Statistical Inference of Associations between Microbial
Communities And host phenoTypes_. doi: 10.18129/B9.bioc.SIAMCAT (URL:
http://doi.org/10.18129/B9.bioc.SIAMCAT), R package version 1.0.1, <URL:
https://bioconductor.org/packages/SIAMCAT/>.
